package JdbcConnection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import FileIO.FileIO;
import FileIO.person;

class Connection {
	static String DB_Driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	static String DB_URL = "jdbc:sqlserver://localhost;integratedSecurity=true";

	public static void main(String[] args) {
		java.sql.Connection conn = null;
		FileIO vFIo = new FileIO("C:\\Users\\USER\\Desktop\\KUYU JAVA\\FileIO\\FileIO.txt");
		ArrayList<person> person;

		try {
			Class.forName(DB_Driver);
			System.out.println("Connecting...");
			conn = DriverManager.getConnection(DB_URL, "LAPTOP-63P4F3SA", "");
			if (conn != null) {
				vFIo.getFileProperties();
				person = vFIo.fileInputStream();
				for (int i = 0; i < person.size(); i++) {
					InsertData(person.get(i), conn);
				}
				ResultSet rst = SearchData("'1001'", "null", "null", 0, conn);
				while (rst.next()) {
					System.out.println(
							rst.getString("empid") + " " + rst.getString("eName") + " " + rst.getString("email"));
				}

				UpdateData("'1001'", "null", "null", 1000, 200, 100, conn);

				DeleteData(rst, conn);
			}

			conn.close();
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	static void InsertData(person p, java.sql.Connection c) {
		Statement stm = null;
		String sql = null;

		try {
			stm = c.createStatement();
			sql = "insert into employee.dbo.employee(";
			sql += " empId";
			sql += " ,eName";
			sql += " ,cName";
			sql += " ,ext";
			sql += " ,height";
			sql += " ,weight";
			sql += " ,email";
			sql += " ,inputtm";
			sql += " ,updattm";
			sql += " ) values(";
			sql += " '" + p.getID() + "'";
			sql += " ,'" + p.getEName() + "'";
			sql += " ,'" + p.getCName() + "'";
			sql += " ," + p.getExt();
			sql += " ," + p.getHeight();
			sql += " ," + p.getWeight();
			sql += " ,'" + p.getEmail() + "'";
			sql += " ,getdate()";
			sql += " ,getdate()";
			sql += ")";

			stm.execute(sql);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	static ResultSet SearchData(String empid, String eName, String cName, int ext, java.sql.Connection c) {
		Statement stm = null;
		String sql = null;
		ResultSet rst = null;

		try {
			stm = c.createStatement();
			sql = "select empid, eName, cName, ext, height, weight, email";
			sql += " from employee.dbo.employee";
			sql += " where empid = isnull( " + empid + ", empid)";
			sql += " and eName = isnull( " + eName + ", eName)";
			sql += " and cName = isnull( " + cName + ", cName)";
			sql += " order by eName";
			if (ext != 0) {
				sql += " and ext = " + ext;
			}

			rst = stm.executeQuery(sql);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return rst;
	}

	static void DeleteData(ResultSet r, java.sql.Connection c) {
		Statement stm = null;
		String sql = null;

		try {
			stm = c.createStatement();

			while (r.next()) {
				sql = "delete from employee.dbo.employee";
				sql += " where empid = " + r.getString("empid");

				stm.execute(sql);
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	static void UpdateData(String empid, String eName, String cName, int ext, int height, int weight,
			java.sql.Connection c) {
		Statement stm = null;
		String sql = null;

		ResultSet r = SearchData(empid, eName, cName, 0, c);

		try {
			stm = c.createStatement();

			while (r.next()) {
				sql = "update employee.dbo.employee";
				sql += " set";
				if (r.getInt("ext") == ext) {
					sql += " ext = " + r.getInt("ext");
				} else {
					sql += " ext = " + ext;
				}
				if (r.getInt("height") == height) {
					sql += " ,height = " + r.getInt("height");
				} else {
					sql += " ,height = " + height;
				}
				if (r.getInt("weight") == weight) {
					sql += " ,weight = " + r.getInt("weight");
				} else {
					sql += " ,weight = " + weight;
				}
				sql += " where empid = " + empid;

				stm.execute(sql);
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
}
