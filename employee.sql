USE [employee]
GO

/****** Object:  Table [dbo].[employee]    Script Date: 3/5/2020 9:12:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[employee](
	[empId] [varchar](10) NULL,
	[eName] [varchar](max) NULL,
	[cName] [varchar](max) NULL,
	[ext] [int] NULL,
	[height] [int] NULL,
	[weight] [int] NULL,
	[email] [varchar](max) NULL,
	[inputtm] [datetime] NULL,
	[updattm] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


